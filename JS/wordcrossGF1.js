// Crucigrama Geografía Fácil n°1
// Seleción de los elementos
let inputLetter32 = document.querySelector('.input32');
let divLetter32 = document.querySelector('.letter32');
let inputLetter33 = document.querySelector('.input33');
let divLetter33 = document.querySelector('.letter33');
let inputLetter34 = document.querySelector('.input34');
let divLetter34 = document.querySelector('.letter34');
let inputLetter35 = document.querySelector('.input35');
let divLetter35 = document.querySelector('.letter35');
let inputLetter36 = document.querySelector('.input36');
let divLetter36 = document.querySelector('.letter36');
let inputLetter37 = document.querySelector('.input37');
let divLetter37 = document.querySelector('.letter37');
let inputLetter38 = document.querySelector('.input38');
let divLetter38 = document.querySelector('.letter38');
let inputLetter39 = document.querySelector('.input39');
let divLetter39 = document.querySelector('.letter39');
let inputLetter40 = document.querySelector('.input40');
let divLetter40 = document.querySelector('.letter40');
let inputLetter41 = document.querySelector('.input41');
let divLetter41 = document.querySelector('.letter41');
let inputLetter42 = document.querySelector('.input42');
let divLetter42 = document.querySelector('.letter42');
let inputLetter43 = document.querySelector('.input43');
let divLetter43 = document.querySelector('.letter43');
let inputLetter44 = document.querySelector('.input44');
let divLetter44 = document.querySelector('.letter44');
let inputLetter45 = document.querySelector('.input45');
let divLetter45 = document.querySelector('.letter45');
let inputLetter46 = document.querySelector('.input46');
let divLetter46 = document.querySelector('.letter46');
let inputLetter47 = document.querySelector('.input47');
let divLetter47 = document.querySelector('.letter47');
let inputLetter48 = document.querySelector('.input48');
let divLetter48 = document.querySelector('.letter48');
let inputLetter49 = document.querySelector('.input49');
let divLetter49 = document.querySelector('.letter49');
let inputLetter50 = document.querySelector('.input50');
let divLetter50 = document.querySelector('.letter50');

let message = document.querySelector('.message');

let sendButton = document.querySelector('.send');
let showClueButton = document.querySelector('.showClue');
let hideClueButton = document.querySelector('.hideClue');

// Declaración de variables
let letter32 = divLetter32.textContent;
let letter33 = divLetter33.textContent;
let letter34 = divLetter34.textContent;
let letter35 = divLetter35.textContent;
let letter36 = divLetter36.textContent;
let letter37 = divLetter37.textContent;
let letter38 = divLetter38.textContent;
let letter39 = divLetter39.textContent;
let letter40 = divLetter40.textContent;
let letter41 = divLetter41.textContent;
let letter42 = divLetter42.textContent;
let letter43 = divLetter43.textContent;
let letter44 = divLetter44.textContent;
let letter45 = divLetter45.textContent;
let letter46 = divLetter46.textContent;
let letter47 = divLetter47.textContent;
let letter48 = divLetter48.textContent;
let letter49 = divLetter49.textContent;
let letter50 = divLetter50.textContent;
inputLetter32.value = '';
inputLetter33.value = '';
inputLetter34.value = '';
inputLetter35.value = '';
inputLetter36.value = '';
inputLetter37.value = '';
inputLetter38.value = '';
inputLetter39.value = '';
inputLetter40.value = '';
inputLetter41.value = '';
inputLetter42.value = '';
inputLetter43.value = '';
inputLetter44.value = '';
inputLetter45.value = '';
inputLetter46.value = '';
inputLetter47.value = '';
inputLetter48.value = '';
inputLetter49.value = '';
inputLetter50.value = '';
let inputLetter;
let findOut32 = true;
let findOut33 = true;
let findOut34 = true;
let findOut35 = true;
let findOut36 = true;
let findOut37 = true;
let findOut38 = true;
let findOut39 = true;
let findOut40 = true;
let findOut41 = true;
let findOut42 = true;
let findOut43 = true;
let findOut44 = true;
let findOut45 = true;
let findOut46 = true;
let findOut47 = true;
let findOut48 = true;
let findOut49 = true;
let findOut50 = true;

// FUNCIONES
// Gestión de cookies
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function endGame() {
    if(findOut32 === false && findOut33 === false && findOut34 === false && findOut35 === false
        && findOut36 === false && findOut37 === false && findOut38 === false && findOut39 === false
        && findOut40 === false && findOut41 === false && findOut42 === false && findOut43 === false
        && findOut44 === false && findOut45 === false && findOut46 === false && findOut47 === false 
        && findOut48 === false && findOut49 === false && findOut50 === false) {
            alert('ENORABUENA ' + readCookie('userName') + ' HAS GANADO !!!')
    } 
    
}

function compareLetter32() {
    inputLetter = (inputLetter32.value).toUpperCase();

    if (inputLetter === letter32) {
        inputLetter32.style.position = "initial";
        divLetter32.style.backgroundColor = 'green';
        findOut32 = false;
    }
    else {
        inputLetter32.style.borderColor = "red";
        inputLetter32.value = '';
    }
}

function compareLetter33() {
    inputLetter = (inputLetter33.value).toUpperCase();

    if (inputLetter === letter33) {
        inputLetter33.style.position = "initial";
        divLetter33.style.backgroundColor = 'green';
        findOut33 = false;
    }
    else {
        inputLetter33.style.borderColor = "red";
        inputLetter33.value = '';
    }    
}

function compareLetter34() {
    inputLetter = (inputLetter34.value).toUpperCase();

    if (inputLetter === letter34) {
        inputLetter34.style.position = "initial";
        divLetter34.style.backgroundColor = 'green';
        findOut34 = false;
    }
    else {
        inputLetter34.style.borderColor = "red";
        inputLetter34.value = '';
    }    
}

function compareLetter35() {
    inputLetter = (inputLetter35.value).toUpperCase();

    if (inputLetter === letter35) {
        inputLetter35.style.position = "initial";
        divLetter35.style.backgroundColor = 'green';
        findOut35 = false;
    }
    else {
        inputLetter35.style.borderColor = "red";
        inputLetter35.value = '';
    }    
}

function compareLetter36() {
    inputLetter = (inputLetter36.value).toUpperCase();

    if (inputLetter === letter36) {
        inputLetter36.style.position = "initial";
        divLetter36.style.backgroundColor = 'green';
        findOut36 = false;
    }
    else {
        inputLetter36.style.borderColor = "red";
        inputLetter36.value = '';
    }    
}

function compareLetter37() {
    inputLetter = (inputLetter37.value).toUpperCase();

    if (inputLetter === letter37) {
        inputLetter37.style.position = "initial";
        divLetter37.style.backgroundColor = 'green';
        findOut37 = false;
    }
    else {
        inputLetter37.style.borderColor = "red";
        inputLetter37.value = '';
    }    
}

function compareLetter38() {
    inputLetter = (inputLetter38.value).toUpperCase();

    if (inputLetter === letter38) {
        inputLetter38.style.position = "initial";
        divLetter38.style.backgroundColor = 'green';
        findOut38 = false;
    }
    else {
        inputLetter38.style.borderColor = "red";
        inputLetter38.value = '';
    }    
}

function compareLetter39() {
    inputLetter = (inputLetter39.value).toUpperCase();

    if (inputLetter === letter39) {
        inputLetter39.style.position = "initial";
        divLetter39.style.backgroundColor = 'green';
        findOut39 = false;
    }
    else {
        inputLetter39.style.borderColor = "red";
        inputLetter39.value = '';
    }    
}

function compareLetter40() {
    inputLetter = (inputLetter40.value).toUpperCase();

    if (inputLetter === letter40) {
        inputLetter40.style.position = "initial";
        divLetter40.style.backgroundColor = 'green';
        findOut40 = false;
    }
    else {
        inputLetter40.style.borderColor = "red";
        inputLetter40.value = '';
    }    
}

function compareLetter41() {
    inputLetter = (inputLetter41.value).toUpperCase();

    if (inputLetter === letter41) {
        inputLetter41.style.position = "initial";
        divLetter41.style.backgroundColor = 'green';
        findOut41 = false;
    }
    else {
        inputLetter41.style.borderColor = "red";
        inputLetter41.value = '';
    }    
}

function compareLetter42() {
    inputLetter = (inputLetter42.value).toUpperCase();

    if (inputLetter === letter42) {
        inputLetter42.style.position = "initial";
        divLetter42.style.backgroundColor = 'green';
        findOut42 = false;
    }
    else {
        inputLetter42.style.borderColor = "red";
        inputLetter42.value = '';
    }    
}

function compareLetter43() {
    inputLetter = (inputLetter43.value).toUpperCase();

    if (inputLetter === letter43) {
        inputLetter43.style.position = "initial";
        divLetter43.style.backgroundColor = 'green';
        findOut43 = false;
    }
    else {
        inputLetter43.style.borderColor = "red";
        inputLetter43.value = '';
    }    
}

function compareLetter44() {
    inputLetter = (inputLetter44.value).toUpperCase();

    if (inputLetter === letter44) {
        inputLetter44.style.position = "initial";
        divLetter44.style.backgroundColor = 'green';
        findOut44 = false;
    }
    else {
        inputLetter44.style.borderColor = "red";
        inputLetter44.value = '';
    }
} 
function compareLetter45() {
    inputLetter = (inputLetter45.value).toUpperCase();
    
    if (inputLetter === letter45) {
        inputLetter45.style.position = "initial";
        divLetter45.style.backgroundColor = 'green';
        findOut45 = false;
    }
    else {
        inputLetter45.style.borderColor = "red";
        inputLetter45.value = '';
    }        
}
function compareLetter46() {
    inputLetter = (inputLetter46.value).toUpperCase();
        
    if (inputLetter === letter46) {
        inputLetter46.style.position = "initial";
        divLetter46.style.backgroundColor = 'green';
        findOut46 = false;
    }
    else {
        inputLetter46.style.borderColor = "red";
        inputLetter46.value = '';
    }        
}
function compareLetter47() {
    inputLetter = (inputLetter47.value).toUpperCase();

    if (inputLetter === letter47) {
        inputLetter47.style.position = "initial";
        divLetter47.style.backgroundColor = 'green';
        findOut47 = false;
    }
    else {
        inputLetter47.style.borderColor = "red";
        inputLetter47.value = '';
    }        
}
function compareLetter48() {
    inputLetter = (inputLetter48.value).toUpperCase();
        
    if (inputLetter === letter48) {
        inputLetter48.style.position = "initial";
        divLetter48.style.backgroundColor = 'green';
        findOut48 = false;
    }
    else {
        inputLetter48.style.borderColor = "red";
        inputLetter48.value = '';
    }        
}
function compareLetter49() {
    inputLetter = (inputLetter49.value).toUpperCase();
        
    if (inputLetter === letter49) {
        inputLetter49.style.position = "initial";
        divLetter49.style.backgroundColor = 'green';
        findOut49 = false;
    }
    else {
        inputLetter49.style.borderColor = "red";
        inputLetter49.value = '';
    }        
}
function compareLetter50() {
    inputLetter = (inputLetter50.value).toUpperCase();
        
    if (inputLetter === letter50) {
        inputLetter50.style.position = "initial";
        divLetter50.style.backgroundColor = 'green';
        findOut50 = false;
    }
    else {
        inputLetter50.style.borderColor = "red";
        inputLetter50.value = '';
    }        
}

function showClue32() {
    if(inputLetter32.style.position = "relative") {
    inputLetter32.style.position = "initial";
    }
}
function showClue33() {
    if(inputLetter33.style.position = "relative") {
    inputLetter33.style.position = "initial";
    }
}
function showClue34() {
    if(inputLetter34.style.position = "relative") {
    inputLetter34.style.position = "initial";
    }
}
function showClue35() {
    if(inputLetter35.style.position = "relative") {
    inputLetter35.style.position = "initial";
    }
}
function showClue36() {
    if(inputLetter36.style.position = "relativel") {
    inputLetter36.style.position = "initial";
    }
}
function showClue37() {
    if(inputLetter37.style.position = "relative") {
    inputLetter37.style.position = "initial";
    }
}
function showClue38() {
    if(inputLetter38.style.position = "relative") {
    inputLetter38.style.position = "initial";
    }
}
function showClue39() {
    if(inputLetter39.style.position = "relative") {
    inputLetter39.style.position = "initial";
    }
}
function showClue40() {
    if(inputLetter40.style.position = "relative") {
        inputLetter40.style.position = "initial";
        }
}
function showClue41() {
    if(inputLetter41.style.position = "relative") {
    inputLetter41.style.position = "initial";
    }
}
function showClue42() {
    if(inputLetter42.style.position = "relative") {
    inputLetter42.style.position = "initial";
    }
}
function showClue43() {
    if(inputLetter43.style.position = "relative") {
    inputLetter43.style.position = "initial";
    }
}
function showClue44() {
    if(inputLetter44.style.position = "relative") {
    inputLetter44.style.position = "initial";
    }
}
function showClue45() {
    if(inputLetter45.style.position = "relative") {
    inputLetter45.style.position = "initial";
    }
}
function showClue46() {
    if(inputLetter46.style.position = "relative") {
    inputLetter46.style.position = "initial";
    }
}
function showClue47() {
    if(inputLetter47.style.position = "relative") {
    inputLetter47.style.position = "initial";
    }
}
function showClue48() {
    if(inputLetter48.style.position = "relative") {
    inputLetter48.style.position = "initial";
    }
}
function showClue49() {
    if(inputLetter49.style.position = "relative") {
    inputLetter49.style.position = "initial";
    }
}
function showClue50() {
    if(inputLetter50.style.position = "relative") {
    inputLetter50.style.position = "initial";
    }
}
function hideClue() {

    inputLetter32.removeEventListener('click', showClue32);
    inputLetter33.removeEventListener('click', showClue33);
    inputLetter34.removeEventListener('click', showClue34);
    inputLetter35.removeEventListener('click', showClue35);
    inputLetter36.removeEventListener('click', showClue36);
    inputLetter37.removeEventListener('click', showClue37);
    inputLetter38.removeEventListener('click', showClue38);
    inputLetter39.removeEventListener('click', showClue39);
    inputLetter40.removeEventListener('click', showClue40);
    inputLetter41.removeEventListener('click', showClue41);
    inputLetter42.removeEventListener('click', showClue42);
    inputLetter43.removeEventListener('click', showClue43);
    inputLetter44.removeEventListener('click', showClue44);
    inputLetter45.removeEventListener('click', showClue45);
    inputLetter46.removeEventListener('click', showClue46);
    inputLetter47.removeEventListener('click', showClue47);
    inputLetter48.removeEventListener('click', showClue48);   
    inputLetter49.removeEventListener('click', showClue49);
    inputLetter50.removeEventListener('click', showClue50);
    if(findOut32) {
    inputLetter32.style.position = "relative";
    }
    if(findOut33) {
    inputLetter33.style.position = "relative";
    }
    if(findOut34) {
        inputLetter34.style.position = "relative";
    }
    if(findOut35) {
        inputLetter35.style.position = "relative";
    }
    if(findOut36) {
        inputLetter36.style.position = "relative";
    }
    if(findOut37) {
        inputLetter37.style.position = "relative";
    }
    if(findOut38) {
        inputLetter38.style.position = "relative";
    }
    if(findOut39) {
        inputLetter39.style.position = "relative";
    }
    if(findOut40) {
        inputLetter40.style.position = "relative";
    }
    if(findOut41) {
        inputLetter41.style.position = "relative";
    }
    if(findOut42) {
        inputLetter42.style.position = "relative";
    }
    if(findOut43) {
        inputLetter43.style.position = "relative";
    }
    if(findOut44) {
        inputLetter44.style.position = "relative";
    }
    if(findOut45) {
        inputLetter45.style.position = "relative";
    }
    if(findOut46) {
        inputLetter46.style.position = "relative";
    }
    if(findOut47) {
        inputLetter47.style.position = "relative";
    }
    if(findOut48) {
        inputLetter48.style.position = "relative";
    } 
    if(findOut49) {
        inputLetter49.style.position = "relative";
    }
    if(findOut50) {
        inputLetter50.style.position = "relative";
    }  
}

function allowShowClue() {
    inputLetter32.addEventListener('click', showClue32);
    inputLetter33.addEventListener('click', showClue33);
    inputLetter34.addEventListener('click', showClue34);
    inputLetter35.addEventListener('click', showClue35);
    inputLetter36.addEventListener('click', showClue36);
    inputLetter37.addEventListener('click', showClue37);
    inputLetter38.addEventListener('click', showClue38);
    inputLetter39.addEventListener('click', showClue39);
    inputLetter40.addEventListener('click', showClue40);
    inputLetter41.addEventListener('click', showClue41);
    inputLetter42.addEventListener('click', showClue42);
    inputLetter43.addEventListener('click', showClue43);
    inputLetter44.addEventListener('click', showClue44);
    inputLetter45.addEventListener('click', showClue45);
    inputLetter46.addEventListener('click', showClue46);
    inputLetter47.addEventListener('click', showClue47);
    inputLetter48.addEventListener('click', showClue48);
    inputLetter49.addEventListener('click', showClue49);
    inputLetter50.addEventListener('click', showClue50);
}

function showMessage() {
    message.style.display = 'block';
}

function hideMessage() {
    message.style.display = 'none';
}

// Evenementos
sendButton.addEventListener('click', compareLetter32);
sendButton.addEventListener('click', compareLetter33);
sendButton.addEventListener('click', compareLetter34);
sendButton.addEventListener('click', compareLetter35);
sendButton.addEventListener('click', compareLetter36);
sendButton.addEventListener('click', compareLetter37);
sendButton.addEventListener('click', compareLetter38);
sendButton.addEventListener('click', compareLetter39);
sendButton.addEventListener('click', compareLetter40);
sendButton.addEventListener('click', compareLetter41);
sendButton.addEventListener('click', compareLetter42);
sendButton.addEventListener('click', compareLetter43);
sendButton.addEventListener('click', compareLetter44);
sendButton.addEventListener('click', compareLetter45);
sendButton.addEventListener('click', compareLetter46);
sendButton.addEventListener('click', compareLetter47);
sendButton.addEventListener('click', compareLetter48);
sendButton.addEventListener('click', compareLetter49);
sendButton.addEventListener('click', compareLetter50);
sendButton.addEventListener('click', endGame);

showClueButton.addEventListener('click', allowShowClue);
showClueButton.addEventListener('mouseover', showMessage);
showClueButton.addEventListener('mouseout', hideMessage);
hideClueButton.addEventListener('click', hideClue);
