// Seleción de los elementos
let inputLetter14 = document.querySelector('.input14');
let divLetter14 = document.querySelector('.letter14');
let inputLetter15 = document.querySelector('.input15');
let divLetter15 = document.querySelector('.letter15');
let inputLetter16 = document.querySelector('.input16');
let divLetter16 = document.querySelector('.letter16');
let inputLetter17 = document.querySelector('.input17');
let divLetter17 = document.querySelector('.letter17');
let inputLetter18 = document.querySelector('.input18');
let divLetter18 = document.querySelector('.letter18');
let inputLetter19 = document.querySelector('.input19');
let divLetter19 = document.querySelector('.letter19');
let inputLetter20 = document.querySelector('.input20');
let divLetter20 = document.querySelector('.letter20');
let inputLetter21 = document.querySelector('.input21');
let divLetter21 = document.querySelector('.letter21');
let inputLetter22 = document.querySelector('.input22');
let divLetter22 = document.querySelector('.letter22');
let inputLetter23 = document.querySelector('.input23');
let divLetter23 = document.querySelector('.letter23');
let inputLetter24 = document.querySelector('.input24');
let divLetter24 = document.querySelector('.letter24');
let inputLetter25 = document.querySelector('.input25');
let divLetter25 = document.querySelector('.letter25');
let inputLetter26 = document.querySelector('.input26');
let divLetter26 = document.querySelector('.letter26');
let inputLetter27 = document.querySelector('.input27');
let divLetter27 = document.querySelector('.letter27');
let inputLetter28 = document.querySelector('.input28');
let divLetter28 = document.querySelector('.letter28');
let inputLetter29 = document.querySelector('.input29');
let divLetter29 = document.querySelector('.letter29');
let inputLetter30 = document.querySelector('.input30');
let divLetter30 = document.querySelector('.letter30');
let inputLetter31 = document.querySelector('.input31');
let divLetter31 = document.querySelector('.letter31');

let message = document.querySelector('.message');

let sendButton = document.querySelector('.send');
let showClueButton = document.querySelector('.showClue');
let hideClueButton = document.querySelector('.hideClue');

// Declaración de variables
let letter14 = divLetter14.textContent;
let letter15 = divLetter15.textContent;
let letter16 = divLetter16.textContent;
let letter17 = divLetter17.textContent;
let letter18 = divLetter18.textContent;
let letter19 = divLetter19.textContent;
let letter20 = divLetter20.textContent;
let letter21 = divLetter21.textContent;
let letter22 = divLetter22.textContent;
let letter23 = divLetter23.textContent;
let letter24 = divLetter24.textContent;
let letter25 = divLetter25.textContent;
let letter26 = divLetter26.textContent;
let letter27 = divLetter27.textContent;
let letter28 = divLetter28.textContent;
let letter29 = divLetter29.textContent;
let letter30 = divLetter30.textContent;
let letter31 = divLetter31.textContent;
inputLetter14.value = '';
inputLetter15.value = '';
inputLetter16.value = '';
inputLetter17.value = '';
inputLetter18.value = '';
inputLetter19.value = '';
inputLetter20.value = '';
inputLetter21.value = '';
inputLetter22.value = '';
inputLetter23.value = '';
inputLetter24.value = '';
inputLetter25.value = '';
inputLetter26.value = '';
inputLetter27.value = '';
inputLetter28.value = '';
inputLetter29.value = '';
inputLetter30.value = '';
inputLetter31.value = '';
let inputLetter;
let findOut14 = true;
let findOut15 = true;
let findOut16 = true;
let findOut17 = true;
let findOut18 = true;
let findOut19 = true;
let findOut20 = true;
let findOut21 = true;
let findOut22 = true;
let findOut23 = true;
let findOut24 = true;
let findOut25 = true;
let findOut26 = true;
let findOut27 = true;
let findOut28 = true;
let findOut29 = true;
let findOut30 = true;
let findOut31 = true;

// Gestión de cookies
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

// Funciones
function endGame() {
    if(findOut14 === false && findOut15 === false && findOut16 === false && findOut17 === false
       && findOut18 === false && findOut19 === false && findOut20 === false && findOut21 === false
       && findOut22 === false && findOut23 === false && findOut24 === false && findOut25 === false
       && findOut26 === false && findOut27 === false && findOut28 === false && findOut29 === false 
       && findOut30 === false && findOut31 === false) {
        alert('ENORABUENA ' + readCookie('userName') + ' HAS GANADO !!!')
    }   
}

function compareLetter14() {
    inputLetter = (inputLetter14.value).toUpperCase();

    if (inputLetter === letter14) {
        inputLetter14.style.position = "initial";
        divLetter14.style.backgroundColor = 'green';
        findOut14 = false;
    }
    else {
        inputLetter14.style.borderColor = "red";
        inputLetter14.value = '';
    }
}

function compareLetter15() {
    inputLetter = (inputLetter15.value).toUpperCase();

    if (inputLetter === letter15) {
        inputLetter15.style.position = "initial";
        divLetter15.style.backgroundColor = 'green';
        findOut15 = false;
    }
    else {
        inputLetter15.style.borderColor = "red";
        inputLetter15.value = '';
    }    
}

function compareLetter16() {
    inputLetter = (inputLetter16.value).toUpperCase();

    if (inputLetter === letter16) {
        inputLetter16.style.position = "initial";
        divLetter16.style.backgroundColor = 'green';
        findOut16 = false;
    }
    else {
        inputLetter16.style.borderColor = "red";
        inputLetter16.value = '';
    }    
}

function compareLetter17() {
    inputLetter = (inputLetter17.value).toUpperCase();

    if (inputLetter === letter17) {
        inputLetter17.style.position = "initial";
        divLetter17.style.backgroundColor = 'green';
        findOut17 = false;
    }
    else {
        inputLetter17.style.borderColor = "red";
        inputLetter17.value = '';
    }    
}

function compareLetter18() {
    inputLetter = (inputLetter18.value).toUpperCase();

    if (inputLetter === letter18) {
        inputLetter18.style.position = "initial";
        divLetter18.style.backgroundColor = 'green';
        findOut18 = false;
    }
    else {
        inputLetter18.style.borderColor = "red";
        inputLetter18.value = '';
    }    
}

function compareLetter19() {
    inputLetter = (inputLetter19.value).toUpperCase();

    if (inputLetter === letter19) {
        inputLetter19.style.position = "initial";
        divLetter19.style.backgroundColor = 'green';
        findOut19 = false;
    }
    else {
        inputLetter19.style.borderColor = "red";
        inputLetter19.value = '';
    }    
}

function compareLetter20() {
    inputLetter = (inputLetter20.value).toUpperCase();

    if (inputLetter === letter20) {
        inputLetter20.style.position = "initial";
        divLetter20.style.backgroundColor = 'green';
        findOut20 = false;
    }
    else {
        inputLetter20.style.borderColor = "red";
        inputLetter20.value = '';
    }    
}

function compareLetter21() {
    inputLetter = (inputLetter21.value).toUpperCase();

    if (inputLetter === letter21) {
        inputLetter21.style.position = "initial";
        divLetter21.style.backgroundColor = 'green';
        findOut21 = false;
    }
    else {
        inputLetter21.style.borderColor = "red";
        inputLetter21.value = '';
    }    
}

function compareLetter22() {
    inputLetter = (inputLetter22.value).toUpperCase();

    if (inputLetter === letter22) {
        inputLetter22.style.position = "initial";
        divLetter22.style.backgroundColor = 'green';
        findOut22 = false;
    }
    else {
        inputLetter22.style.borderColor = "red";
        inputLetter22.value = '';
    }    
}

function compareLetter23() {
    inputLetter = (inputLetter23.value).toUpperCase();

    if (inputLetter === letter23) {
        inputLetter23.style.position = "initial";
        divLetter23.style.backgroundColor = 'green';
        findOut23 = false;
    }
    else {
        inputLetter23.style.borderColor = "red";
        inputLetter23.value = '';
    }    
}

function compareLetter24() {
    inputLetter = (inputLetter24.value).toUpperCase();

    if (inputLetter === letter24) {
        inputLetter24.style.position = "initial";
        divLetter24.style.backgroundColor = 'green';
        findOut24 = false;
    }
    else {
        inputLetter24.style.borderColor = "red";
        inputLetter24.value = '';
    }    
}

function compareLetter25() {
    inputLetter = (inputLetter25.value).toUpperCase();

    if (inputLetter === letter25) {
        inputLetter25.style.position = "initial";
        divLetter25.style.backgroundColor = 'green';
        findOut25 = false;
    }
    else {
        inputLetter25.style.borderColor = "red";
        inputLetter25.value = '';
    }    
}

function compareLetter26() {
    inputLetter = (inputLetter26.value).toUpperCase();

    if (inputLetter === letter26) {
        inputLetter26.style.position = "initial";
        divLetter26.style.backgroundColor = 'green';
        findOut26 = false;
    }
    else {
        inputLetter26.style.borderColor = "red";
        inputLetter26.value = '';
    }    
}

function compareLetter27() {
    inputLetter = (inputLetter27.value).toUpperCase();

    if (inputLetter === letter27) {
        inputLetter27.style.position = "initial";
        divLetter27.style.backgroundColor = 'green';
        findOut27 = false;
    }
    else {
        inputLetter27.style.borderColor = "red";
        inputLetter27.value = '';
    }    
}

function compareLetter28() {
    inputLetter = (inputLetter28.value).toUpperCase();

    if (inputLetter === letter28) {
        inputLetter28.style.position = "initial";
        divLetter28.style.backgroundColor = 'green';
        findOut28 = false;
    }
    else {
        inputLetter28.style.borderColor = "red";
        inputLetter28.value = '';
    }    
}

function compareLetter29() {
    inputLetter = (inputLetter29.value).toUpperCase();

    if (inputLetter === letter29) {
        inputLetter29.style.position = "initial";
        divLetter29.style.backgroundColor = 'green';
        findOut29 = false;
    }
    else {
        inputLetter29.style.borderColor = "red";
        inputLetter29.value = '';
    }    
}

function compareLetter30() {
    inputLetter = (inputLetter30.value).toUpperCase();

    if (inputLetter === letter30) {
        inputLetter30.style.position = "initial";
        divLetter30.style.backgroundColor = 'green';
        findOut30 = false;
    }
    else {
        inputLetter30.style.borderColor = "red";
        inputLetter30.value = '';
    }    
}

function compareLetter31() {
    inputLetter = (inputLetter31.value).toUpperCase();

    if (inputLetter === letter31) {
        inputLetter31.style.position = "initial";
        divLetter31.style.backgroundColor = 'green';
        findOut31 = false;
    }
    else {
        inputLetter31.style.borderColor = "red";
        inputLetter31.value = '';
    }    
}

function showClue14() {
    if(inputLetter14.style.position = "relative") {
    inputLetter14.style.position = "initial";
    }
}
function showClue15() {
    if(inputLetter15.style.position = "relative") {
    inputLetter15.style.position = "initial";
    }
}
function showClue16() {
    if(inputLetter16.style.position = "relative") {
    inputLetter16.style.position = "initial";
    }
}
function showClue17() {
    if(inputLetter17.style.position = "relative") {
    inputLetter17.style.position = "initial";
    }
}
function showClue18() {
    if(inputLetter18.style.position = "relativel") {
    inputLetter18.style.position = "initial";
    }
}
function showClue19() {
    if(inputLetter19.style.position = "relative") {
    inputLetter19.style.position = "initial";
    }
}
function showClue20() {
    if(inputLetter20.style.position = "relative") {
    inputLetter20.style.position = "initial";
    }
}
function showClue21() {
    if(inputLetter21.style.position = "relative") {
    inputLetter21.style.position = "initial";
    }
}
function showClue22() {
    if(inputLetter22.style.position = "relative") {
    inputLetter22.style.position = "initial";
    }
}
function showClue23() {
    if(inputLetter23.style.position = "relative") {
    inputLetter23.style.position = "initial";
    }
}
function showClue24() {
    if(inputLetter24.style.position = "relative") {
    inputLetter24.style.position = "initial";
    }
}
function showClue25() {
    if(inputLetter25.style.position = "relative") {
    inputLetter25.style.position = "initial";
    }
}
function showClue26() {
    if(inputLetter26.style.position = "relative") {
    inputLetter26.style.position = "initial";
    }
}
function showClue27() {
    if(inputLetter27.style.position = "relative") {
    inputLetter27.style.position = "initial";
    }
}
function showClue28() {
    if(inputLetter28.style.position = "relative") {
    inputLetter28.style.position = "initial";
    }
}
function showClue29() {
    if(inputLetter29.style.position = "relative") {
    inputLetter29.style.position = "initial";
    }
}
function showClue30() {
    if(inputLetter30.style.position = "relative") {
    inputLetter30.style.position = "initial";
    }
}
function showClue31() {
    if(inputLetter31.style.position = "relative") {
    inputLetter31.style.position = "initial";
    }
}

function hideClue() {

    inputLetter14.removeEventListener('click', showClue14);
    inputLetter15.removeEventListener('click', showClue15);
    inputLetter16.removeEventListener('click', showClue16);
    inputLetter17.removeEventListener('click', showClue17);
    inputLetter18.removeEventListener('click', showClue18);
    inputLetter19.removeEventListener('click', showClue19);
    inputLetter20.removeEventListener('click', showClue20);
    inputLetter21.removeEventListener('click', showClue21);
    inputLetter22.removeEventListener('click', showClue22);
    inputLetter23.removeEventListener('click', showClue23);
    inputLetter24.removeEventListener('click', showClue24);
    inputLetter25.removeEventListener('click', showClue25);
    inputLetter26.removeEventListener('click', showClue26);
    inputLetter27.removeEventListener('click', showClue27); 
    inputLetter28.removeEventListener('click', showClue28); 
    inputLetter29.removeEventListener('click', showClue29); 
    inputLetter30.removeEventListener('click', showClue30); 
    inputLetter31.removeEventListener('click', showClue31);    
    if(findOut14) {
    inputLetter14.style.position = "relative";
    }
    if(findOut15) {
    inputLetter15.style.position = "relative";
    }
    if(findOut16) {
        inputLetter16.style.position = "relative";
    }
    if(findOut17) {
        inputLetter17.style.position = "relative";
    }
    if(findOut18) {
        inputLetter18.style.position = "relative";
    }
    if(findOut19) {
        inputLetter19.style.position = "relative";
    }
    if(findOut20) {
        inputLetter20.style.position = "relative";
    }
    if(findOut21) {
        inputLetter21.style.position = "relative";
    }
    if(findOut22) {
        inputLetter22.style.position = "relative";
    }
    if(findOut23) {
        inputLetter23.style.position = "relative";
    }
    if(findOut24) {
        inputLetter24.style.position = "relative";
    }
    if(findOut25) {
        inputLetter25.style.position = "relative";
    }
    if(findOut26) {
        inputLetter26.style.position = "relative";
    }
    if(findOut27) {
        inputLetter27.style.position = "relative";
    }
    if(findOut28) {
        inputLetter28.style.position = "relative";
    }
    if(findOut29) {
        inputLetter29.style.position = "relative";
    }
    if(findOut30) {
        inputLetter30.style.position = "relative";
    }
    if(findOut31) {
        inputLetter31.style.position = "relative";
    }
        
}

function allowShowClue() {
    inputLetter14.addEventListener('click', showClue14);
    inputLetter15.addEventListener('click', showClue15);
    inputLetter16.addEventListener('click', showClue16);
    inputLetter17.addEventListener('click', showClue17);
    inputLetter18.addEventListener('click', showClue18);
    inputLetter19.addEventListener('click', showClue19);
    inputLetter20.addEventListener('click', showClue20);
    inputLetter21.addEventListener('click', showClue21);
    inputLetter22.addEventListener('click', showClue22);
    inputLetter23.addEventListener('click', showClue23);
    inputLetter24.addEventListener('click', showClue24);
    inputLetter25.addEventListener('click', showClue25);
    inputLetter26.addEventListener('click', showClue26);
    inputLetter27.addEventListener('click', showClue27);
    inputLetter28.addEventListener('click', showClue28);
    inputLetter29.addEventListener('click', showClue29);
    inputLetter30.addEventListener('click', showClue30);
    inputLetter31.addEventListener('click', showClue31);
}

function showMessage() {
    message.style.display = 'block';
}

function hideMessage() {
    message.style.display = 'none';
}


// Evenementos
sendButton.addEventListener('click', compareLetter14);
sendButton.addEventListener('click', compareLetter15);
sendButton.addEventListener('click', compareLetter16);
sendButton.addEventListener('click', compareLetter17);
sendButton.addEventListener('click', compareLetter18);
sendButton.addEventListener('click', compareLetter19);
sendButton.addEventListener('click', compareLetter20);
sendButton.addEventListener('click', compareLetter21);
sendButton.addEventListener('click', compareLetter22);
sendButton.addEventListener('click', compareLetter23);
sendButton.addEventListener('click', compareLetter24);
sendButton.addEventListener('click', compareLetter25);
sendButton.addEventListener('click', compareLetter26);
sendButton.addEventListener('click', compareLetter27);
sendButton.addEventListener('click', compareLetter28);
sendButton.addEventListener('click', compareLetter29);
sendButton.addEventListener('click', compareLetter30);
sendButton.addEventListener('click', compareLetter31);
sendButton.addEventListener('click', endGame);

showClueButton.addEventListener('click', allowShowClue);
showClueButton.addEventListener('mouseover', showMessage);
showClueButton.addEventListener('mouseout', hideMessage);
hideClueButton.addEventListener('click', hideClue);